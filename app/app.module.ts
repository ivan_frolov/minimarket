import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { HttpModule }    from '@angular/http';

import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from "./app.component";
import { NavigationComponent } from "./navigation.component";
import { ProductsListComponent } from "./products-list.component";
import {ProductComponent} from "./product.component";


const routes: Routes = [
	{
		path: "",
		component: AppComponent
	}
];

@NgModule({
	imports: [ 
		BrowserModule,
		/*RouterModule.forRoot(routes, { useHash: true }),*/
		HttpModule
	],
	declarations: [
		AppComponent,
		NavigationComponent,
		ProductsListComponent,
		ProductComponent
	],
	bootstrap: [AppComponent]
})

export class AppModule {

}