export class Product {
	constructor(
		public name: string,
		public shortDescription: string,
		public fullDescription: string,
		public price: number,
		public categoryId: number
	) {
		this.name = name;
		this.shortDescription = shortDescription;
		this.fullDescription = fullDescription;
		this.price = price;
		this.categoryId = categoryId;
	}

	public static build = (data: any): Product => {
		return new Product(
			data.name,
			data.short_description,
			data.full_description,
			data.price,
			data.categoryId
		);
	}
}