import {Component, Input} from "@angular/core";
import {Product} from "./models/Products";
import {__moduleName} from "./app.component";

@Component({
    moduleId: __moduleName,
    selector: "products-list",
    templateUrl: "products-list.component.html"
})

export class ProductsListComponent {
    @Input() products: Product[] = null;
}