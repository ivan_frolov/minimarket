var gulp = require("gulp");

var serverPath = "../../php/minimarket/app";

gulp.task("html", function () {
    return gulp.src('app/**/*.html')
        	.pipe(gulp.dest(serverPath));
});