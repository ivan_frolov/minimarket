var gulp = require("gulp");
var del = require("del");

var serverPath = "../../php/minimarket";
var appPath = serverPath + "/app";
var assetsPath = serverPath + "/assets";

gulp.task("clean-all", function () {
    return del([appPath, assetsPath], {force: true}).then(paths => {
    	console.log('Deleted files and folders:\n', paths.join('\n'));
	});
});