import { Component, Input } from "@angular/core";
import {Product} from "./models/Products";
import {__moduleName} from "./app.component";

@Component({
    moduleId: __moduleName,
    selector: "product",
    templateUrl: "app/product.component.html"
})

export class ProductComponent {
    @Input() product: Product = null;
}