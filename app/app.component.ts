import {Component, OnInit} from "@angular/core";
import {ProductsService} from "./services/products.service";
import {Product} from "./models/Products";
import {module} from "@angular/upgrade/src/angular_js";
export declare const __moduleName: string;


@Component({
	moduleId: __moduleName,
	selector: "minimarket",
	templateUrl: "app.component.html",
	providers: [ProductsService]
})

export class AppComponent implements OnInit{
	public products: Product[] = [];

	constructor(
		private productsService: ProductsService
	) {

	}

	ngOnInit(): void {
		this.getProducts();
	}


	private getProducts = (): void => {
		this.productsService.getProducts()
			.then(result => this.products = result);
	}

}