import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import {Product} from "../models/Products";

import 'rxjs/add/operator/toPromise';

@Injectable()

export class ProductsService{
	private CONTROLLER_BASE_URL: string = "/api/index.php/products";

	constructor(
		private http: Http
	) {

	}
	getProducts(): Promise<Product[]> {
		let method = "/get";
		return this.http.get(this.CONTROLLER_BASE_URL + method)
			.toPromise()
			.then(response =>
				 response.json().map(Product.build) as Product[]
			)
	};
}
