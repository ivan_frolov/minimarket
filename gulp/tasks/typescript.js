var gulp = require("gulp");
var ts = require("gulp-typescript");
var tsProject = ts.createProject("tsconfig.json");
var sourcemaps = require("gulp-sourcemaps");

var serverPath = "../../php/minimarket/app";

gulp.task("ts", function () {
    tsProject.src()
        .pipe(sourcemaps.init())
        .pipe(tsProject()).js
        .pipe(sourcemaps.write(/*{sourceRoot: serverPath}*/))
        .pipe(gulp.dest(serverPath));
});